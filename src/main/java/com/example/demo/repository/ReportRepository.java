package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	// ReportRepository が JpaRepository を継承しており、findAllメソッドを実⾏しているため、
	// こちらで特に何か記載する必要はありません
	// JpaRepositry にはあらかじめいくつかのメソッドが定義されており、SQL⽂を打つ必要がありません。
	// ちなみに、findAllで実⾏されている処理はSQL⽂の select * from report; のようにイメージしてもらえれば⼤丈夫です。
	// 今回、様々な JpaRepository のメソッドを使うので、課題に⼊る前に調べておくといいでしょう。
	List<Report> findByCreatedDateBetween(Date startr, Date end);
	// List<Report> findByCreatedDateBetween(Date startr, Date end, Sort sort);
	// List<Report> findOrderByCreatedDateDescByCreatedDateBetween(Date startr, Date end);
}