package com.example.demo.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@RequestMapping("/")
@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表⽰画⾯
	@GetMapping
	public ModelAndView top(@RequestParam(name = "start", required = false) String start, @RequestParam(name = "end", required = false) String end) {
		ModelAndView mav = new ModelAndView();

		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport(start, end);
		Collections.sort(contentData, Comparator.comparing(Report::getUpdatedDate).reversed());

		// 画⾯遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);

		// コメントを全件取得
		// コメントデータオブジェクトを保管
		List<Comment> commentData = commentService.findAllComment();
		Collections.sort(commentData, Comparator.comparing(Comment::getCreated_date));
		mav.addObject("comments", commentData);

		return mav;
	}

	// 新規投稿画⾯
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		// form⽤の空のentityを準備
		Report report = new Report();

		// 画⾯遷移先を指定
		mav.setViewName("/new");

		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// オブジェクトに日付を設定
		report.setCreatedDate(new Date());
		report.setUpdatedDate(new Date());
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除
	@DeleteMapping("delete/{id}")
	public ModelAndView deleteReport(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@PostMapping("/edit")
	public ModelAndView editContent(@ModelAttribute("formModel") Report report) {
		ModelAndView mav = new ModelAndView();
		// ①SELECTで取り直す、②遷移前の一覧画面のデータを引き継ぐ
		// 画⾯遷移先を指定
		mav.setViewName("/edit");
		// entityを保管
		mav.addObject("formModel", report);

		return mav;
	}
	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateReport (@PathVariable Integer id, @ModelAttribute("formModel") Report updateReport) {
		Report report = reportService.getReport(id);
		report.setContent(updateReport.getContent());
		report.setUpdatedDate(new Date());

		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//
	//コメント
	//

	// コメント投稿
	@PostMapping("/comment")
	public ModelAndView newComment(@ModelAttribute("formModel") Comment comment) {
		comment.setCreated_date(new Date());
		comment.setUpdated_date(new Date());
		// 投稿をテーブルに格納
		commentService.saveComment(comment);

		// レポート更新日時編集
		Report report = reportService.getReport(comment.getReport_id());
		report.setUpdatedDate(comment.getUpdated_date());
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント編集画面
	@RequestMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Comment comment = commentService.getComment(id);
		// 編集する投稿をセット
		mav.addObject("formModel", comment);
		// 画⾯遷移先を指定
		mav.setViewName("/editComment");

		return mav;
	}

	// コメント編集処理
	@PutMapping("/updateComment/{id}")
	// @RequestMapping(value = "/updateComment/{id}", method = RequestMethod.PUT)
	// @PostMapping("/updateComment/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Comment updateComment) {
		Comment comment = commentService.getComment(id);
		comment.setContent(updateComment.getContent());
		comment.setUpdated_date(new Date());
		// 編集した投稿を更新
		commentService.saveComment(comment);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}
}
