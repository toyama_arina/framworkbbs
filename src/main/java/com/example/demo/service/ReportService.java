package com.example.demo.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport(String start, String end) {
		Date startDate = null;
		Date endDate = null;

        if(StringUtils.isBlank(start)) {
        	start ="2020/01/01 00:00:00";
        } else {
        	start = start.replace("-", "/");
        	start += " 00:00:00";
        }
        if(StringUtils.isBlank(end)) {
        	end = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" ).format(new Date());
        } else {
        	end = end.replace("-", "/");
        	end += " 23:59:59";
        }
        // String型⇒Date型に変換
        try{
        	SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

        	startDate = sdFormat.parse(start);
/*            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.add(Calendar.HOUR_OF_DAY, 9);
            startDate = calendar.getTime();*/

        	endDate = sdFormat.parse(end);
/*            calendar.setTime(endDate);
            calendar.add(Calendar.HOUR_OF_DAY, 9);
            endDate = calendar.getTime();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

		 return reportRepository.findByCreatedDateBetween(startDate, endDate);
	}

	// レコード1件取得
	public Report getReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコードの削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}
}
